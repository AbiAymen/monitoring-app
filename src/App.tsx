import React, { useState } from 'react';
import './App.css';
import GlobalView from './Components/GlobalView'; 
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import orange from '@material-ui/core/colors/orange';
import red from '@material-ui/core/colors/red';
import grey from '@material-ui/core/colors/grey';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Login from './Components/Login';
import { blue, cyan } from '@material-ui/core/colors';


const Darktheme = createMuiTheme({
  palette: {
    primary: {
      main: grey[900],
    },
    secondary: {
      main: grey[200],
    },
    text : {
      primary : grey[50]
    }
  },
});
const Server_Endpoint = 'http://4c08a071dcb7.ngrok.io/'
const Cors_proxy = 'https://cors-anywhere.herokuapp.com/';
function App() {
  const[theme,SetTheme] = useState(true);
  const[FirstColor, SetFirstColor] = useState(blue[500]);
  const[SecondColor, SetSecondColor] = useState(cyan[500]);
  const[TextColor, SetTextColor] = useState(grey[900]);

const Maintheme = createMuiTheme({
  palette: {
    primary: {
      main: FirstColor,
      light : grey[50]
    },
    secondary: {
      main: SecondColor,
    },
    text : {
      primary : TextColor
    }
  },
});
  
  return (
    <ThemeProvider theme={theme ? Maintheme : Darktheme}>
      <Router>
        <div data-testid="global_view">
          <Switch>
            <Route path="/dashboard">
            <GlobalView
                
                firstColor={FirstColor}
                secondColor={SecondColor}
                textColor = {TextColor}
                setFirstColor={SetFirstColor}
                setSecondColor={SetSecondColor}
                setTextColor={SetTextColor}
                theme={theme}
                ThemeSwitcher={SetTheme}
                Url={Server_Endpoint}
                proxy={Cors_proxy}
              />
            </Route>
            <Route path="/"
            >
              <Login />
              
            </Route>
          </Switch>
        </div>
      </Router>
    </ThemeProvider>
  );
}
export default App;
