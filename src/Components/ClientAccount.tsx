import React, { useEffect, useState } from 'react';
import {withStyles, createStyles, Theme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { TablePagination } from '@material-ui/core';

function createData(
  id: string,
  idAppli: string,
  tidNid: string,
) {
  return {
    id,
    idAppli,
    tidNid,
  };
}
const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);
const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:nth-of-type(even)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }),
)(TableRow);
function Row(props: { row: ReturnType<typeof createData> }) {
  const { row } = props;
  return (
    <React.Fragment>
      <StyledTableRow>
        <StyledTableCell component="th" scope="row" align="left">
          {row.id}
        </StyledTableCell>
        <StyledTableCell align="center">{row.idAppli}</StyledTableCell>
        <StyledTableCell align="center">{row.tidNid}</StyledTableCell>
      </StyledTableRow>
      <TableRow>
        <TableCell
          style={{ paddingBottom: 0, paddingTop: 0 }}
          colSpan={6}
        ></TableCell>
      </TableRow>
    </React.Fragment>
  );
}
export default function Account(props:{AccountsService : any, language :boolean}) {
  const [table, setTable] = useState([
    createData("CLI 0", "0", "0"),
  ]);
  const [tableUpdated, SetTableUpdated] = useState(false)
  const [page , SetPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  useEffect(() => {
    props.AccountsService(page,rowsPerPage).then((res: { json: () => any; }) => res.json()).then((res: { content: { id: string; idAppli: string; tidNid: string; }[]; }) => {
      var midTable :{ id: string; idAppli: string; tidNid: string}[] = [];
  
      res.content.forEach((element: { id: string; idAppli: string; tidNid: string}) => {
        
        midTable.push(createData(element.id, element.idAppli, element.tidNid))
       
      });
      
      setTable(midTable);
      SetTableUpdated(!tableUpdated)
    })
  }, [page,rowsPerPage])
  return (
    <div>
      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead>
            <StyledTableRow>
              <StyledTableCell align="left">CLI</StyledTableCell>
              <StyledTableCell align="center">ID Appli</StyledTableCell>
              <StyledTableCell align="center">Tid_Nid</StyledTableCell>
            </StyledTableRow>
          </TableHead>
          <TableBody>
            {table.map((row) => (
              <Row key={row.id} row={row} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 20]}
        component="div"
        count={1000}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={(event, newPage) => {
          SetPage(newPage);
        }}
        onChangeRowsPerPage={(event) => {
          setRowsPerPage(+event.target.value);
          SetPage(0);
        }}
      />
    </div>
  );
}