import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { TextField, Toolbar, Button} from '@material-ui/core';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    margin : 'auto',
    width : '50%',
    marginTop : "10%",
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    marginLeft :'15%',
    width :'100%'
  },
  cover: {
    width: '50%',
  },
  row: {
    display : 'flex',
    justifyContent : 'flex-start',
    width: '100%'
  }
}));
export default function Login() {
  const classes = useStyles();
  const history = useHistory();
  
  
  return (
    <Card className={classes.root} elevation={3}>
      <div className={classes.row}>
        <CardMedia className={classes.cover} image="test2.jpg" />
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography component="h5" variant="h5">
              Login
            </Typography>
            <Toolbar />
            <TextField
              style={{ width: "100%" }}
              id="outlined-basic"
              label="Username"
              variant="outlined"
            />
            <Toolbar />
            <TextField
              style={{ width: "100%" }}
              id="standard-password-input"
              label="Password"
              type="password"
              variant="outlined"
            />
            <Toolbar />
           
            <Button
              style={{ width: "100%" }}
              variant="contained"
              color="primary"
              size="large"
              onClick={()=> {history.push("/dashboard");}}
            >
              Login
            </Button>
            
          </CardContent>
        </div>
      </div>
    </Card>
  );
}
