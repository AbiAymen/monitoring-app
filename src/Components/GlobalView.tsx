import React, { useState } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import ToolBar from './AppBar';
import MainView from './MainView';
import { Tabs, Tab, Box, Divider } from "@material-ui/core";
import MatricesUpdate from './MatricesUpdate';
import TableChartIcon from '@material-ui/icons/TableChart';
import TocIcon from '@material-ui/icons/Toc';
import Account from './ClientAccount';
import SettingsIcon from '@material-ui/icons/Settings';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import Settings from './Settings';
import FilesService from '../Services/FilesService';
import AccountsService from '../Services/AccountsService';



const drawerWidth = 240;
interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      backgroundColor : theme.palette.primary.light
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      background: "linear-gradient(to right bottom,theme.palette.primary.light,#ffffff00)",
      color: "red"
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      backgrounColor : theme.palette.primary.light
    },
    drawerPaper: {
      width: drawerWidth,
      backgrounColor : theme.palette.primary.light
    },
   content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    tabs: {
      borderRight: `1px solid ${theme.palette.divider}`,
    },
  }),
);


function TabPanel(props: TabPanelProps) {
  const { children, value, index } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
    >
      {value === index && (
        <Box p={3}>
          <Typography component={'span'}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}
function a11yProps(index: any) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}


export default function GlobalView(props :{ThemeSwitcher: any , theme : boolean , Url : string , proxy : string , firstColor : string , secondColor : string , textColor : string , setFirstColor : any , setSecondColor : any , setTextColor : any}) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [language , SetLanguage] = useState(true);
  const [InputPath , SetInputPath] = useState('');
  const [OutputPath , SetOutputPath] = useState('');
  const [ArchivePath , SetArchivePath] = useState('');

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <ToolBar
          SetLanguage={SetLanguage}
          language={language}
          firstColor={props.firstColor}
        />
      </AppBar>

      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <Toolbar />
        <div>
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={value}
            onChange={handleChange}
            aria-label="Vertical tabs example"
            className={classes.tabs}
          >
            <Tab
              label={
                <div
                  style={{
                    display: "flex",
                    justifyContent: "left",
                    alignItems: "center",
                    width: "100%",
                  }}
                >
                  <TocIcon style={{ marginRight: "20px" }} />
                  <Typography color="textPrimary" variant="body2">
                    {language ? "Files" : "Fichiers"}
                  </Typography>
                </div>
              }
              {...a11yProps(0)}
            />
            <Divider variant="middle" />
            <Tab
              label={
                <div
                  style={{
                    display: "flex",
                    justifyContent: "left",
                    alignItems: "center",
                    width: "100%",
                  }}
                >
                  <TableChartIcon style={{ marginRight: "20px" }} />
                  <Typography color="textPrimary" variant="body2">
                    Matrices
                  </Typography>
                </div>
              }
              {...a11yProps(1)}
            />
            <Divider variant="middle" />
            <Tab
              label={
                <div
                  style={{
                    display: "flex",
                    justifyContent: "left",
                    alignItems: "center",
                    width: "100%",
                  }}
                >
                  <AccountBoxIcon style={{ marginRight: "20px" }} />
                  <Typography color="textPrimary" variant="body2">
                    {language ? "Accounts" : "Comptes"}
                  </Typography>
                </div>
              }
              {...a11yProps(2)}
            />
            <Divider variant="middle" />
            <Tab
              label={
                <div
                  style={{
                    display: "flex",
                    justifyContent: "left",
                    alignItems: "center",
                    width: "100%",
                  }}
                >
                  <SettingsIcon style={{ marginRight: "20px" }} />
                  <Typography color="textPrimary" variant="body2">
                    {language ? "Settings" : "Parametres"}
                  </Typography>
                </div>
              }
              {...a11yProps(2)}
            />
          </Tabs>
        </div>
      </Drawer>

      <main className={classes.content}>
        <Toolbar />
        <TabPanel value={value} index={0}>
          <MainView
            FilesService={FilesService}
            language={language}
            InputPath={InputPath}
            OutputPath={OutputPath}
            ArchivePath={ArchivePath}
            SetInputPath={SetInputPath}
            SetOutputPath={SetOutputPath}
            SetArchivePath={SetArchivePath}
          />
        </TabPanel>
        <TabPanel value={value} index={1}></TabPanel>
        <TabPanel value={value} index={2}>
          <MatricesUpdate
            proxy={props.proxy}
            Url={props.Url}
            language={language}
          />
        </TabPanel>
        <TabPanel value={value} index={3}>
          {" "}
          <Settings
            firstColor={props.firstColor}
            secondColor={props.secondColor}
            textColor={props.textColor}
            setFirstColor={props.setFirstColor}
            setSecondColor={props.setSecondColor}
            setTextColor={props.setTextColor}
            InputPath={InputPath}
            OutputPath={OutputPath}
            ArchivePath={ArchivePath}
            SetInputPath={SetInputPath}
            SetOutputPath={SetOutputPath}
            SetArchivePath={SetArchivePath}
          />
        </TabPanel>
        <TabPanel value={value} index={4}>
          <Account AccountsService={AccountsService} language={language} />
        </TabPanel>
        <TabPanel value={value} index={5}></TabPanel>
        <TabPanel value={value} index={6}>
          {" "}
          <Settings
            firstColor={props.firstColor}
            secondColor={props.secondColor}
            textColor={props.textColor}
            setFirstColor={props.setFirstColor}
            setSecondColor={props.setSecondColor}
            setTextColor={props.setTextColor}
            InputPath={InputPath}
            OutputPath={OutputPath}
            ArchivePath={ArchivePath}
            SetInputPath={SetInputPath}
            SetOutputPath={SetOutputPath}
            SetArchivePath={SetArchivePath}
          />
        </TabPanel>
      </main>
    </div>
  );
}