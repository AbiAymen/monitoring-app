import React, { useState, useEffect } from 'react';
import { Typography, TextField, Button, Paper, Toolbar, Snackbar, Table, TableHead, TableCell, TableBody, TableRow } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Alert from '@material-ui/lab/Alert';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import RepertoireService from '../Services/DirectorySettingsService';
import CircularProgress from '@material-ui/core/CircularProgress';
import DirectoryUpdateSerivce from '../Services/DirectoryUpdateService';


export default function Settings(props : {firstColor : string , secondColor : string , textColor : string , setFirstColor : any , setSecondColor : any , setTextColor : any, InputPath : string , OutputPath : string , ArchivePath : string , SetInputPath : any , SetOutputPath : any , SetArchivePath : any}) {

  const [open, setOpen] = useState(false);
  const [InputPath , SetInputPath] = useState(props.InputPath);
  const [OutputPath , SetOutputPath] = useState(props.OutputPath);
  const [ArchivePath , SetArchivePath] = useState(props.ArchivePath);
  const [InputExist, SetInputExist] = useState(false);
  const [OutputExist, SetOutputExist] = useState(false);
  const [ArchiveExist, SetArchiveExist] = useState(false);
  const [InputLoading , SetInputLoading] = useState(true);
  const [OutputLoading , SetOutputLoading] = useState(true);
  const [ArchiveLoading , SetArchiveLoading] = useState(true);
  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };



  useEffect(()=>{
    
    RepertoireService(InputPath).then((res)=>{
     SetInputExist(res)
     SetInputLoading(false)
    })
    RepertoireService(OutputPath).then((res)=>{
      SetOutputExist(res)
      SetOutputLoading(false)
     })
     RepertoireService(ArchivePath).then((res)=>{
      SetArchiveExist(res)
      SetArchiveLoading(false)
     })
  },[])

  return (
    <div>
      <Paper elevation={3} style={{ marginBottom: "10px", padding: "15px" }}>
      <Snackbar open={open} autoHideDuration={30000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="success">
          Settings updated succesfully !
          </Alert>
        </Snackbar>
        <Typography variant="h5">Settings</Typography>
        <Toolbar/>
        <Typography variant="h6">Working directories</Typography>
        <Table>
          <TableHead>
            <TableRow>
            <TableCell>Directory</TableCell>
            <TableCell>Path</TableCell>
            <TableCell>Exist</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>Input</TableCell>
              <TableCell>{InputPath}</TableCell>
              <TableCell>{InputLoading ? <CircularProgress size={30}/> : <div> {InputExist ? <CheckCircleIcon color="inherit" style={{color:'green'}}/> : <ErrorIcon color='inherit' style={{color:'red'}}/> }</div>}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Output</TableCell>
              <TableCell>{OutputPath}</TableCell>
              <TableCell>{OutputLoading ? <CircularProgress size={30}/> : <div>{OutputExist ? <CheckCircleIcon color="inherit" style={{color:'green'}}/> : <ErrorIcon color='inherit' style={{color:'red'}}/> }</div>}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Archive</TableCell>
              <TableCell>{ArchivePath}</TableCell>
              <TableCell>{ArchiveLoading ? <CircularProgress size={30}/> : <div>{ArchiveExist ? <CheckCircleIcon color="inherit" style={{color:'green'}}/> : <ErrorIcon color='inherit' style={{color:'red'}}/> }</div>}</TableCell>
            </TableRow>
          </TableBody>
       
        </Table>
        <Toolbar />
        <div
          style={{
            paddingTop: "10px",
            paddingBottom: "10px",
            display: "flex",
            justifyContent: "space-around",
          }}
        >
        <ClickAwayListener onClickAway={()=>{
          
          RepertoireService(InputPath).then((res)=>{
          SetInputExist(res)
          SetInputLoading(false)
        })}}>
        <TextField id="outlined-basic" label="Input" variant="outlined" onChange={(e)=>{SetInputPath(e.target.value)
        SetInputLoading(true)}}/>
        </ClickAwayListener>
        <ClickAwayListener onClickAway={()=>{
          
          RepertoireService(OutputPath).then((res)=>{
          SetOutputExist(res)
          SetOutputLoading(false)
          
        })}}>
        <TextField id="outlined-basic" label="Output" variant="outlined" onChange={(e)=>{SetOutputPath(e.target.value)
        SetOutputLoading(true)}}/>
        </ClickAwayListener>
        <ClickAwayListener onClickAway={()=>{
          
          RepertoireService(ArchivePath).then((res)=>{
          SetArchiveExist(res)
          SetArchiveLoading(false)
        })
        }}>
        <TextField id="outlined-basic" label="Archive" variant="outlined" onChange={(e)=>{SetArchivePath(e.target.value)
        SetArchiveLoading(true)}}/>
        </ClickAwayListener>
          <Button
            color="primary"
            variant="contained"
            size="small"
            onClick={() => {
            if(InputExist && OutputExist && ArchiveExist){
              DirectoryUpdateSerivce(InputPath, OutputPath , ArchivePath).then((res)=>{
                console.log(res)
              })
            }
           setOpen(true)}}
          >
            Save
          </Button>
        </div>
      </Paper>
    </div>
  );
}

/*<Typography variant="h6">Theme :</Typography>
      <Typography variant="body1">First color :</Typography>
      <CirclePicker onChange={(e)=>{props.setFirstColor(e.hex);}}/>
      <Typography variant="body1">Second color :</Typography>
      <CirclePicker onChange={(e)=>{props.setSecondColor(e.hex);}}/>*/

      const foldersList = [
        "Folder 1",
        "Folder 2",
        "Folder 3",
        "Folder 4",
        "Folder 5",
        "Folder 6",
        "Folder 7",
        "Folder 8",
        "Folder 9",
        "Folder 10",
        "Folder 11",
        "Folder 12",
      ]