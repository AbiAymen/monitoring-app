import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

let container = null;
beforeEach(() => {
  // met en place un élément DOM comme cible de rendu
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // nettoie en sortie de test
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("affiche les données utilisateur", async () => {
  const fakeUser = {
    name: "Joni Baez",
    age: "32",
    address: "123, Charming Avenue"
  };
  var body : BodyInit = JSON.stringify(fakeUser);
  jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve(
     new Response(body)
    )
  );

  // Utilise la version asynchrone de `act` pour appliquer les promesses accomplies


  // retire la simulation pour assurer une bonne isolation des tests
  //global.fetch.mockRestore();
});