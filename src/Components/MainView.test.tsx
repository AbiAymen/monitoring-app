import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import MainView from './MainView';

let container: Element | DocumentFragment;
beforeEach(() => {

  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  
  unmountComponentAtNode(container);
 
});

it("affiche les données utilisateur", async () => {

    var midTable: {
        fileName: string;
        dateTrait: string;
        applicationSource: string;
        status: boolean;
      }[] = [
          {fileName : "Bkcli", dateTrait: "13/08/2020",applicationSource : "APPCTR" ,status : true},
          {fileName : "Bkcom", dateTrait : "05/05/2019",applicationSource : "APPCTR" ,status : false},
          {fileName : "BkAdcli", dateTrait : "10/04/2019",applicationSource : "APPCTR" ,status : false},
          {fileName : "Bkhepar", dateTrait : "03/01/2020",applicationSource : "APPCTR" ,status : true},
          {fileName : "Bkbcli", dateTrait : "03/08/2020",applicationSource : "APPCTR" ,status : false},
          {fileName : "Bktedcli", dateTrait : "18/03/2020",applicationSource : "APPCTR" ,status : true},
          {fileName : "Bkerpcli", dateTrait : "05/08/2020",applicationSource : "APPCTR" ,status : true},
          {fileName : "Bktest", dateTrait : "09/07/2019",applicationSource : "APPCTR" ,status : false},
          {fileName : "Bkjest", dateTrait : "11/09/2020",applicationSource : "APPCTR" ,status : false},

      ];

  
  
  


  const FilesServiceMock = jest.fn();
  FilesServiceMock.mockImplementation((page : number , rowPerPage : number ) =>
  Promise.resolve(
    {content : midTable}
  ))

  await act(async () => {
    render(<MainView FilesService={FilesServiceMock} language={true} InputFoler='folder1' OutputFolder='folder2' ArchiveFolder='folder3'/>, container);
  });
   midTable.forEach((element)=>{
    expect(container.textContent).toContain(element.fileName);
    expect(container.textContent).toContain(element.applicationSource);
    expect(container.textContent).toContain(element.dateTrait);
    
   })
   

});