import React, { useEffect, useState } from 'react';
import {withStyles, createStyles, Theme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from "@material-ui/core/Paper";
import DoneIcon from '@material-ui/icons/Done';
import ClearIcon from '@material-ui/icons/Clear';
import { TablePagination, Button, Typography, LinearProgress, Divider, Collapse, IconButton, Checkbox} from '@material-ui/core';
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded';
import HourglassFullRoundedIcon from '@material-ui/icons/HourglassFullRounded';
import LoopIcon from '@material-ui/icons/Loop';
import FolderIcon from '@material-ui/icons/Folder';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import FilesService from '../Services/FilesService';
import FilesListService from '../Services/FilesListService';
import DirectoryNamesService from '../Services/DirectoryNamesService';
import SendingFilesListSerivce from '../Services/SendingFilesListService';



function createData(
  filename: string,
  Date: string,
  entity: string,
  input : string,
  output : string
) {
  return {
    filename,
    Date,
    entity,
    input,
    output
  };
}
const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);
const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:nth-of-type(even)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }),
)(TableRow);



   


function Row(props: { row: ReturnType<typeof createData> , file : string}) {
  const [open, SetOpen] = useState(false)



 
  const { row } = props;
  return (
    <React.Fragment>
      <StyledTableRow >
        <StyledTableCell component="th" scope="row" align="left">
          {row.filename}
        </StyledTableCell>
        <StyledTableCell align="center">{row.Date}</StyledTableCell>
        
        <StyledTableCell align="center">{row.entity}</StyledTableCell>
       <StyledTableCell align="center"><IconButton aria-label="expand row" size="small" onClick={() => SetOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton></StyledTableCell>
        <StyledTableCell>
        <IconButton aria-label="expand row" size="small" onClick={() => SetOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </StyledTableCell>
        
      </StyledTableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={8}>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <div style={{maxHeight :"250px" , overflowY :'scroll'}}>
  <Typography>{row.input}</Typography>
           </div>
        
        </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}
export default function MainView(props : {FilesService : any , language : boolean , InputPath : string , OutputPath : string , ArchivePath : string , SetInputPath : any , SetOutputPath : any , SetArchivePath :any}) {
  const [table, setTable] = useState([
    createData("", "", "", "",""),
  ]);
  const [tableUpdated, SetTableUpdated] = useState(false)
  const [page , SetPage] = useState(168);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [start,SetStart] = useState(false)
  const [loading , SetLoading] = useState(true)
  const [file,Setfile] = useState('')
  const [filesTable, SetFilesTable] = useState([{path : ''}]);
  
  var indexTable :number[]= [];
  
 function onStart(){
   var midtable = filesTable;
  indexTable.forEach((i)=>{
    midtable.splice(i,1)
  })
  SetFilesTable(midtable)
  console.log(filesTable)
  SendingFilesListSerivce(filesTable)
 }


useEffect(() => {  
  DirectoryNamesService().then((res)=>{
    res.json().then((res)=>{
      console.log(res.content)
      props.SetInputPath(res.content[0].repIn)
      props.SetOutputPath(res.content[0].repOut)
      props.SetArchivePath(res.content[0].repArchiv)
    })
  })
  
  
  
  
  
  FilesListService()
  .then((res)=>res.json())
  .then((res)=>{
    console.log(res)
    var midtable : {path : string}[]=[];
    res.forEach((element: { path: string; })=>{
      midtable.push(element)
    })
    SetFilesTable(midtable)
  })
                     
                   if(start) 
                    {SendingFilesListSerivce(filesTable)
                      .then((res: { json: () => any; }) => res.json())
                      .then((res: { applicationSource: string; dateTrait: string; fileName: string; status: boolean; fileblob :string }[]) => {
                        console.log(res)
                        
                        var midTable: {
                          filename: string;
                          Date: string;
                          entity: string;
                          input : string;
                          output : string
                          
                        }[] = [];
                        SetLoading(false)
                        res.forEach(
                          (element: {
                            applicationSource: string;
                            dateTrait: string;
                            fileName: string;
                            fileblob : string;
                          }) => {
                            
                          
                            
                            

                            midTable.push(
                              createData(
                                element.fileName,
                                element.dateTrait,
                                element.applicationSource,
                               atob(element.fileblob),
                               atob(element.fileblob)
                              )
                            );
                          }
                        );
                        setTable(midTable);
                        SetTableUpdated(!tableUpdated);
                      });
                  }}, [page,rowsPerPage,start])
  return (
    <div>
      <Paper elevation={3} style={{ marginBottom: "10px", padding: "15px" }}>
        <div
          style={{
            paddingTop: "10px",
            paddingBottom: "10px",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <div>
            <Typography variant="h6">Input folder :</Typography>
            <div style={{ display: "flex" , alignItems:"center"}}>
              <FolderIcon style={{marginRight:"5px"}}/>
              <Typography variant="body1">{props.InputPath}</Typography>
            </div>
          </div>
         
          <div>
            <Typography variant="h6">Output folder :</Typography>
            <div style={{ display: "flex" , alignItems:"center" }}>
              <FolderIcon style={{marginRight:"5px"}}/>
              <Typography variant="body1">{props.OutputPath}</Typography>
            </div>
          </div>
         
          <div>
            <Typography variant="h6">Archive folder :</Typography>
            <div style={{ display: "flex", alignItems:"center" }}>
              <FolderIcon style={{marginRight:"5px"}}/>
              <Typography variant="body1">{props.ArchivePath}</Typography>
            </div>
          </div>
          <Button
            color="primary"
            variant="contained"
            size="small"
            onClick={() => {
              SetStart(!start);
              //onStart()
            }}
          >
            {start ? "Stop" : "Start"}
          </Button>
        </div>
        {start ? (
          <div>
            {loading ? (
              <div style={{ display: "flex", justifyContent: "flex-start" }}>
                <HourglassFullRoundedIcon
                  color="inherit"
                  style={{ color: "orange", paddingRight: "5px" }}
                />
                <Typography color="inherit" style={{ color: "orange" }}>
                  Loading...
                </Typography>
              </div>
            ) : (
              <div style={{ display: "flex", justifyContent: "flex-start" }}>
                <CheckCircleRoundedIcon
                  color="inherit"
                  style={{ color: "green", paddingRight: "5px" }}
                />
                <Typography color="inherit" style={{ color: "green" }}>
                  Running
                </Typography>
              </div>
            )}
          </div>
        ) : (
          <div></div>
        )}
      </Paper>
      {start ? (
        <div style={{ margin: "auto" }}>
          {loading ? (
            <div>
              <LinearProgress />
            </div>
          ) : (
            <div>
              <TableContainer component={Paper}>
                <Table aria-label="collapsible table">
                  <TableHead>
                    <StyledTableRow>
                      <StyledTableCell align="left">
                        {props.language ? "File" : "Fichier"}
                      </StyledTableCell>
                      <StyledTableCell align="center">Date</StyledTableCell>
                      
                      <StyledTableCell align="center">
                        {props.language ? "Entity" : "Entité"}
                      </StyledTableCell>
                      <StyledTableCell align="center">Input</StyledTableCell>
                      <StyledTableCell align="center">
                        Output
                      </StyledTableCell>
                  
                    </StyledTableRow>
                  </TableHead>
                  <TableBody>
                    {table.map((row) => (
                      <Row key={row.filename} row={row} file={file}/>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 20]}
                component="div"
                count={1000}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={(event, newPage) => {
                  SetPage(newPage);
                }}
                onChangeRowsPerPage={(event) => {
                  setRowsPerPage(+event.target.value);
                  SetPage(0);
                }}
              />
            </div>
          )}
        </div>
      ) : (
        <div>
          <Table>
            <TableHead>
              <StyledTableRow>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell>File</StyledTableCell>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell></StyledTableCell>
                
              </StyledTableRow>
            </TableHead>
            <TableBody>
             {filesTable.map((element)=>(
               <StyledTableRow>
                 <StyledTableCell> <Checkbox
                        defaultChecked
                          onChange={(e)=>{if(!e.target.checked){
                        if(!indexTable.includes(filesTable.indexOf(element)))
                        {indexTable.push(filesTable.indexOf(element))}}
                        else if (e.target.checked){
                          if(indexTable.includes(filesTable.indexOf(element))){
                            indexTable.splice(indexTable.indexOf(filesTable.indexOf(element)),1)
                          }
                        }
                        console.log(indexTable)}}
                        /></StyledTableCell>
             <StyledTableCell>{element.path}</StyledTableCell>
                 <StyledTableCell></StyledTableCell>
                 <StyledTableCell></StyledTableCell>
                
               </StyledTableRow>
             ))}
            </TableBody>
          </Table>
        </div>
      )}
    </div>
  );
}