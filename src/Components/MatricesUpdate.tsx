import React, { useEffect, useState} from 'react';
import { TextField, Button, NativeSelect, Typography, Paper, Snackbar, TableRow, TableCell, TableBody, Table, TableHead,Toolbar, TableContainer } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Alert from '@material-ui/lab/Alert';


export default function MatricesUpdate( props : {Url : string , proxy : string , language : boolean}) {
  const [fieldNumber, SetFieldNumber] = useState(0)
  const [midValue, SetMidValue] = useState(0);
  const [filename, SetFilename] = useState('');
  const [inputValue, SetInputValue] = useState([['', '', '']]);
  const [open, setOpen] = useState(false);
  const [fileMatrix,SetFileMatrix] = useState([{id : 0 ,champName : "", action : "" , valeur : ""}])
  const [filesName,SetFilesName] = useState([{matriceName : ''}])
  const [updatingMatrix, SetUpdatingMatrix] = useState(false);
  const [fieldChanging , SetFieldChanging] = useState(false);
  const [fileId , SetFileId] =  useState(0);
  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };
  function handleChange(n: number, input: string, i: number) {
    var midTable = inputValue;
    midTable[n][i] = input;
    SetInputValue(midTable);
  }
  // request sender 
  function fireRequest(url: string, matrixName: string, request: string ,modifNumber: number, champs: { "id": number, "champName": string, "action": string, "valeur": string }[]) {
    console.log(champs)
    fetch(url, {
      method: request,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        id: 202,
        matriceName: matrixName,
        nbrChampSrc: 5,
        nbrChampDest: 7,
        nbrModif: modifNumber,
        champs: champs,
      }),
    })
      .then((res) => {
        res.json();
        SetFieldChanging(!fieldChanging);
        setOpen(true);
      })
  }
  function findId(inputValue : string  , oldTable : { "id": number, "champName": string, "action": string, "valeur": string }[] ){
    var j =0 ; 
    var id = 0;
    console.log(oldTable.length)
    for(j=0 ; j < oldTable.length ; j++){
     if(inputValue===oldTable[j].champName){
         id=oldTable[j].id
      }
    }
    console.log(id)
    return id
  }
  useEffect(() => {
    fetch(`${props.proxy}${props.Url}matrices`, { method: 'GET',}).then((res) => res.json()).then((res) => {
              var midTable = filesName
              res.content.forEach((element: { matriceName: string; }) => {
                midTable.push({matriceName : element.matriceName})
              });
              SetFilesName(midTable)
             })}, [])
             useEffect(()=>{
  if(filename!==""){
    fetch(`${props.proxy}${props.Url}matrices/${filename}`, { method: 'GET',}).then((res) => res.json()).then((res) => {
    SetFileMatrix(res.champs)
    SetFileId(res.id);
   })
  }
},[filename,fieldChanging])
  return (
    <div>
      <Paper elevation={1} style={{ padding: "20px" }}>
        <Snackbar open={open} autoHideDuration={30000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="success">
            {props.language ? 'Matrix sent succesfully !' : 'Matrice envoyé avec succés !'}
          </Alert>
        </Snackbar>
  <Typography variant="h4">{props.language ? 'Matrices update form' : 'Formulaire de mise à jour des matrices'}</Typography>
        <Toolbar />
        <Typography variant="h6">{props.language ? 'Choose a matrix :' :'Choisir une matrice :'}</Typography>
        <div style={{height:"10px"}}></div>
        <Autocomplete
          id="combo-box-demo"
          options={filesName}
          getOptionLabel={(option: { matriceName: any }) => option.matriceName}
          style={{ width: 300 }}
          onChange={(event, value) => {
            SetFilename(value === null ? "" : value.matriceName);
          }}
          renderInput={(
            params:
              | (JSX.IntrinsicAttributes &
                  import("@material-ui/core").StandardTextFieldProps)
              | (JSX.IntrinsicAttributes &
                  import("@material-ui/core").FilledTextFieldProps)
              | (JSX.IntrinsicAttributes &
                  import("@material-ui/core").OutlinedTextFieldProps)
          ) => <TextField {...params} label="Matrix" variant="outlined" />}
        />
        {filename === "" ? (
          <div></div>
        ) : (
          <div style={{ marginTop: "20px" }}>
            <Typography style={{ marginRight: "20px" }} variant="h5">
             {props.language ?  'File :':'Fichier :'}{filename}
            </Typography>
            <div style={{height:"10px"}}></div>
            <TableContainer component={Paper}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell style={{ fontWeight: "bold" }} align="left">
                      {props.language ? 'Field' : 'Champ'}
                    </TableCell>
                    <TableCell style={{ fontWeight: "bold" }} align="center">
                      Action
                    </TableCell>
                    <TableCell style={{ fontWeight: "bold" }} align="center">
                      {props.language ? 'Value' : 'Valeur'}
                    </TableCell>
                    <TableCell ></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {fileMatrix.map((e, i) => (
                    <TableRow>
                      <TableCell align="left">
                        {fileMatrix[i].champName}
                      </TableCell>
                      <TableCell align="center">
                        {fileMatrix[i].action}
                      </TableCell>
                      <TableCell align="center">
                        {fileMatrix[i].valeur}
                      </TableCell>
                      <TableCell>
                        <Button
                          color="primary"
                          onClick={() => {
                            fetch(`${props.proxy}${props.Url}matrices/${fileId}/champs/${findId(fileMatrix[i].champName,fileMatrix)}`, { method: 'DELETE',}).then((res)=>{
              SetFieldChanging(!fieldChanging)
                            })
                          }}
                        >
                          {props.language ?  'Delete' : 'Supprimer'}
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <Toolbar />
            {fileMatrix ===
            [{ id: 0, champName: "", action: "", valeur: "" }] ? (
              <Button
                variant="outlined"
                size="small"
                color="primary"
                onClick={() => {
                  SetUpdatingMatrix(true);
                }}
              >
                {props.language ? 'Add matrix' : 'Ajouter une matrice'}
              </Button>
            ) : (
              <Button
                variant="outlined"
                size="small"
                color="primary"
                onClick={() => {
                  SetUpdatingMatrix(true);
                }}
              >
                {props.language ? 'Update matrix' : 'Mettre à jour'}
              </Button>
            )}
            <div style={{height:"10px"}}></div>
            {updatingMatrix ? (
            <Paper elevation={2} style={{ padding: "20px" }}>
              {updatingMatrix ? (
                <div>
                  <Typography variant="h6">{props.language ? 'Choose the number of changes :' : 'Choisir le nombre de changements :'}</Typography>
                  <div style={{height:"10px"}}></div>
                  <TextField
                    id="outlined-basic"
                    variant="outlined"
                    size="small"
                    style={{ marginTop: "20px", marginRight: "20px" }}
                    onChange={(event) => {
                      SetMidValue(parseInt(event.target.value));
                    }}
                  />
                  <Button
                    style={{ marginTop: "20px" }}
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      SetFieldNumber(midValue);
                      var i;
                      for (i = 0; i < midValue; i++) {
                        inputValue[i] = ["", "", ""];
                      }
                    }}
                  >
                    {props.language ? 'Add' : 'Ajouter'}
                  </Button>
                </div>
              ) : (
                <div></div>
              )}
              <div style={{height:"15px"}}></div>
              {[...Array(fieldNumber)].map((e, i) => (
                <div
                  style={{ display: "flex", justifyContent: "space-around" }}
                >
                  <TextField
                    defaultValue="champ"
                    size="small"
                    variant="outlined"
                    onChange={(e) => handleChange(i, e.target.value, 0)}
                    style={{ marginTop: "10px" }}
                  />
                  <NativeSelect
                    inputProps={{ "aria-label": "age" }}
                    onChange={(e) => handleChange(i, e.target.value, 1)}
                  >
                    <option value="">Action</option>
                    <option value={"Calculable"}>Calculable</option>
                    <option value={"Fige"}>Fige</option>
                    <option value={"Remplacer"}>Remplacer</option>
                  </NativeSelect>
                  <TextField
                    size="small"
                    onChange={(e) => handleChange(i, e.target.value, 2)}
                    variant="outlined"
                    style={{ marginTop: "10px" }}
                  />
                </div>
              ))}
              {fieldNumber !== 0 ? (
                <div>
                  <div style={{height:"15px"}}></div>
                  <Button
                    style={{ marginLeft: "45%" }}
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      console.log(inputValue);
                      var champs: {
                        id: number;
                        champName: string;
                        action: string;
                        valeur: string;
                      }[] = [];
                      inputValue.map(
                        (e, i) =>
                          (champs[i] = {
                            id: findId(inputValue[i][0], fileMatrix),
                            champName: inputValue[i][0],
                            action: inputValue[i][1],
                            valeur: inputValue[i][2],
                          })
                      );
                      fireRequest(
                        `${props.proxy}${props.Url}matrices/${fileId}`,
                        filename,
                        "PUT",
                        fieldNumber,
                        champs
                      );
                    }}
                  >
                    {props.language ? 'Submit' : 'Soumettre'}
                  </Button>
                </div>
              ) : (
                <div></div>
              )}
            </Paper>)  : <div></div>}
          </div>
        )}
      </Paper>
    </div>
  );
}
/*const files = [
  { title: 'Bkcli', matrix :[{champName :"2" ,action :"fige",valeur : "5"},{champName :"2" ,action :"fige",valeur : "5"}]  },
  { title: 'Bkcom', matrix :[{champName :"2" ,action :"Calculable",valeur : "5"},{champName :"2" ,action :"Calculable",valeur : "5"}]  },
  { title: 'Bkhypar', matrix :[{champName :"2" ,action :"Remplacer",valeur : "5"},{champName :"2" ,action :"Remplacer",valeur : "5"}]   },
  { title: 'Bkt', matrix :[{champName :"2" ,action :"Remplacer",valeur : "5"},{champName :"2" ,action :"Remplacer",valeur : "5"}]},
];*/