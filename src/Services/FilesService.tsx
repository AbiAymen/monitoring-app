import { EndPoint } from './ServicesConfig';
import {Proxy } from './ServicesConfig';

export default function FilesService(page : number , rowsPerPage : number){
    return fetch(
        `${Proxy}${EndPoint}/repertoires/parser`,
        { method: "GET" }
      )
      /*return Promise.resolve(
        {content : midTable})*/
}

const midTable: {
    fileName: string;
    dateTrait: string;
    applicationSource: string;
    status: boolean;
  }[] = [
      {fileName : "Bkcli", dateTrait: "13/08/2020",applicationSource : "APPCTR" ,status : true},
      {fileName : "Bkcom", dateTrait : "05/05/2019",applicationSource : "APPCTR" ,status : false},
      {fileName : "BkAdcli", dateTrait : "10/04/2019",applicationSource : "APPCTR" ,status : false},
      {fileName : "Bkhepar", dateTrait : "03/01/2020",applicationSource : "APPCTR" ,status : true},
      {fileName : "Bkbcli", dateTrait : "03/08/2020",applicationSource : "APPCTR" ,status : false},
];