import {EndPoint , Proxy} from './ServicesConfig'

export default function AccountsService(page : number , rowsPerPage : number){
    return fetch(`${Proxy}${EndPoint}clients?page=${page}&size=${rowsPerPage}`, { method: 'GET' })
}