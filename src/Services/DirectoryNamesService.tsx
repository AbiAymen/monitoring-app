import { EndPoint } from './ServicesConfig';
import {Proxy } from './ServicesConfig';

export default function DirectoryNamesService(){
    return fetch(
        `${Proxy}${EndPoint}repertoires/repertoires`,
        { method: "GET" }
      )
      /*return Promise.resolve(
        {content : midTable})*/
}
