import { EndPoint } from './ServicesConfig';
import {Proxy } from './ServicesConfig';

export default function FilesListService(){
    return fetch(
        `${Proxy}${EndPoint}repertoires/getFileList`,
        { method: "GET" }
      )
      /*return Promise.resolve(
        {content : midTable})*/
}
