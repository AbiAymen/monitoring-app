import { EndPoint } from './ServicesConfig';
import {Proxy } from './ServicesConfig';

export default function RepertoireExistService(path : string){
    return fetch(
        `${Proxy}${EndPoint}repertoires/exist?path=${path}`,
        { method: "GET" }
      ).then((res)=>{
          return res.json()
      })
      
}

export function DirectoryUpdateSerivce(input : string , output : string , archive : string){
    return fetch(`|${Proxy}${EndPoint}repertoires/1`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
         repIn : input,
         repOut : output,
         repArchiv : archive
        }),
      })
        
}