import { EndPoint } from './ServicesConfig';
import {Proxy } from './ServicesConfig';


export default function SendingFilesListSerivce(filesTable : {path  :string} []){
    return fetch(`${Proxy}${EndPoint}repertoires/parser`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(
         filesTable
        ),
      })
        
}